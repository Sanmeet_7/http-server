# importing necessary modules
import configparser
import socket
import threading
import sys
import time
import os
import random   # to set time for 'Retry After' header
import csv   # for keeping record of POST method
import uuid   # to generate unique id for cookie
import logging   # for log files

# to read config.ini file
CONFIG = None

# get current GMT time (in HTTP format - rfc1123)
def get_current_GMTtime():
    return time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime())

# get current local time (in HTTP format)
def get_current_localtime():
    return time.strftime('%a, %d %b %Y %H:%M:%S +0530', time.localtime())

# get no. of seconds since epoch
def get_seconds_since_epoch(time_string):
    time_struct = time.strptime(time_string, "%a, %d %b %Y %H:%M:%S GMT")
    return time.mktime(time_struct)

# function to handle 'Max-Age' parameter of 'Set-Cookie' header
# cookies get deleted after 'Max-Age' is over
def handle_cookies():
    global CONFIG
    fields = []
    rows = []
    try:
        # continuously run this function in separate thread
        while True:
            # if cookie file exist, then read it, check the date-time in it
            # if it is beyond 'Max-Age' supported by server, then remove it
            # write updated data into cookie file
            if os.access("mycookies.csv", os.F_OK):
                fp = open("mycookies.csv", "r")
                csv_reader_obj = csv.reader(fp)
                # fields = next(csv_reader_obj)
                for each_row in csv_reader_obj:
                    rows.append(each_row)
                fp.close()
                # fields = rows[0]
                indices = []
                for i in range(1, len(rows)):
                    cookie_date_time = rows[i][1]
                    current_date_time = get_current_GMTtime()
                    cookie_seconds_since_epoch = get_seconds_since_epoch(cookie_date_time)
                    current_seconds_since_epoch = get_seconds_since_epoch(current_date_time)

                    if (current_seconds_since_epoch - cookie_seconds_since_epoch) >= int(CONFIG['COOKIE']['MAX_AGE']):
                        indices.append(i)
                
                for j in range(len(indices)):
                    rows.pop(indices[j])

                fp = open("mycookies.csv", "w")
                csv_writer_obj = csv.writer(fp)
                csv_writer_obj.writerow(fields)
                csv_writer_obj.writerows(rows)
                fp.close()
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, "-", err))


# function to set cookie
def set_cookie(CLIENT_IP):
    global CONFIG
    fields = ['client_ip', 'date_time', 'cookie']
    rows = []
    try: 
        # if cookie data file alredy exists
        if os.access("mycookies.csv", os.F_OK):
            fp = open("mycookies.csv", "r")
            csv_reader_obj = csv.reader(fp)

            for row in csv_reader_obj:
                rows.append(row)
            fp.close()
            for each_row in rows:
                # if cookie already present in csv file, then return its value without generating new cookie
                if str(each_row[0]) == str(CLIENT_IP):
                    cookie_val = each_row[2]

                    # calculate remaining age of cookie in seconds, and set 'Max-Age' parameter
                    cookie_date_time = each_row[1]
                    current_date_time = get_current_GMTtime()

                    cookie_seconds_since_epoch = get_seconds_since_epoch(cookie_date_time)
                    current_seconds_since_epoch = get_seconds_since_epoch(current_date_time)
                    remaining_age = int(CONFIG['COOKIE']['MAX_AGE']) - int(current_seconds_since_epoch - cookie_seconds_since_epoch)

                    if remaining_age <= 0:
                        remaining_age = 0

                    return "cookie_name=" + cookie_val + "; Max-Age=" + str(remaining_age) + "; SameSite=Strict"
            
            # if cookie is not in storage, then create new one, store in csv file and return it
            fp = open("mycookies.csv", "a")
            csv_writer_obj = csv.writer(fp)
            new_cookie_value = str(uuid.uuid4())
            csv_writer_obj.writerow([CLIENT_IP, get_current_GMTtime(), new_cookie_value])
            fp.close()
            return "cookie_name=" + new_cookie_value + "; Max-Age=" + CONFIG['COOKIE']['MAX_AGE'] + "; SameSite=Strict"

        # otherwise, create new cookie file, create new cookie, add to storage and return
        else:
            new_cookie_value = str(uuid.uuid4())
            fp = open("mycookies.csv", "w")
            csv_writer_obj = csv.writer(fp)
            csv_writer_obj.writerow(fields)
            csv_writer_obj.writerow([CLIENT_IP, get_current_GMTtime(), new_cookie_value])
            fp.close()
            return "cookie_name=" + new_cookie_value + "; Max-Age=" + CONFIG['COOKIE']['MAX_AGE'] + "; SameSite=Strict"
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, "-", err))


# get last modification time of file with given path
def get_last_modification_time(path=""):
    try:
        # For Unix, the epoch is January 1, 1970, 00:00:00 (UTC)
        seconds_since_epoch = os.path.getmtime(path)
        return time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(seconds_since_epoch))
    # otherwise it raises OSError
    except OSError as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, "-", err))
        return None

# handle percent encoding in post requests of Content-Type = application/x-www-form-urlencoded
def decode_percent_encoded_data(value=None):
    i = 0
    decoded_value = ""
    try:
        while i < len(value):
            # if '%' is encountered then get next two characters (hex), convert them into bytes, decode them and store into decoded_value
            if value[i] == "%":
                p_enc = value[i+1:i+3]
                bytesdata = bytes.fromhex(p_enc)
                decoded_value += bytesdata.decode("ASCII")
                i += 2
            # if '+' is encountered then add a space into decoded_value
            elif value[i] == "+":
                decoded_value += " "
            # otherwise just add alphanumeric character as it is
            else:
                decoded_value += value[i]
            i += 1
        return decoded_value
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, "-", err))

# parsing POST method's multipart/form-data -> returns a list ['name', 'filename', 'value']
def parse_multipart_data(request_headers, request_body):
    try:
        boundary = ""
        if "Content-Type" in request_headers:
            boundary = request_headers["Content-Type"].split('boundary=')[1].strip('"')
    
        new_body = "\r\n\r\n".join(request_body)
        new_body2 = new_body.split('--' + boundary)
        lst = []
        for each in new_body2:
            if "\r\n\r\n" in each:
                lst.append(each.split("\r\n\r\n"))
    
        a = None #name
        b = None #filename
        c = None #value
        final_list = []
        for i in range(len(lst)):
            b = None
            if 'name=' in lst[i][0]:
                a = lst[i][0].split('name=')[1].strip('"')
                if '"' in a:
                    a = a.split('"')[0]
                c = lst[i][1].strip('\r\n')
            if 'filename=' in lst[i][0]:
                b = lst[i][0].split('filename=')[1].strip('"')
                if '"' in b:
                    b = b.split('"')[0]

            final_list.append([a,b,c])

        return final_list
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, "-", err))

# status codes used
STATUS_CODE_MESSAGES = {'100':'Continue', '200': 'OK', '201': 'Created', '301': 'Moved Permanently', '304': 'Not Modified', '400': 'Bad Request', '403': 'Forbidden', '404': 'Not Found', '405': 'Method Not Allowed', '414':'Request-URI Too Long', '417':'Expectation Failed', '501': 'Not Implemented', '503': 'Service Unavailable', '505': 'HTTP Version not supported'}



# handling GET method
def GET_method(CLIENT_IP, request_path, request_version, request_headers):
    global CONFIG, STATUS_CODE_MESSAGES
    # flag
    is_image_file = False
    doc_root_path = CONFIG['ROOT']['PATH']
    status_code = 200

    # get relative file path
    if request_path == "":
        return
    file_name = ""
    if request_path == '/':
        file_name = "/index.html"
    else:
        file_name = request_path
    file_name = doc_root_path + file_name

    try:
        # check for file existence and access (here for GET, read access)
        if os.access(file_name, os.F_OK) and not os.access(file_name, os.R_OK):
            # create forbidden response
            status_code = 403
            entity = "<!DOCTYPE html><head><title>Access Error</title></head><body><h1>403 Forbidden</h1><p>Access denied</p></body></html>"
            response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
            response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
            response += "Date: " + get_current_GMTtime() + "\r\n"
            response += "Connection: close\r\n"
            response += "Content-Length: " + str(len(entity)) + "\r\n" 
            response += "Content-Language: en-US\r\n"
            response += "Content-Type: text/html\r\n"
            response += "\r\n" + entity
            response_encoded = response.encode()
            user_agent = "-"
            if "User-Agent" in request_headers:
                user_agent = request_headers["User-Agent"]
            logging.info(" {} \"GET {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, len(entity), user_agent))
            return response, response_encoded
        
        # compare time in 'If-Modified-Since' header and file's modification time
        if os.access(file_name, os.F_OK) and "If-Modified-Since" in request_headers:
            t1 = time.strptime(get_last_modification_time(file_name).strip(), "%a, %d %b %Y %H:%M:%S GMT")
            t2 = time.strptime(request_headers["If-Modified-Since"].strip(), "%a, %d %b %Y %H:%M:%S GMT")
            if t1 < t2:
                status_code = 304
                response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
                response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
                response += "Date: " + get_current_GMTtime() + "\r\n"
                response += "Connection: close\r\n"
                response += "Set-Cookie: " + set_cookie(CLIENT_IP) + "\r\n"
                response += "\r\n"
                response_encoded = response.encode()
                # write into log file
                user_agent = "-"
                if "User-Agent" in request_headers:
                    user_agent = request_headers["User-Agent"]
                logging.info(" {} \"GET {} {}\" {} \"-\" \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, user_agent))
                return response, response_encoded
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))
    try:
        # if someone requests notfound.html files
        if request_path == "/notfound.html":
            raise FileNotFoundError
        # to handle image files
        if file_name.split('.')[-1] in ['jpg', 'jpeg', 'png', 'webp', 'gif']:
            fp = open(file_name, "rb")
            is_image_file = True
        else:
            fp = open(file_name, "r")
    except FileNotFoundError as err:
        # write into log file
        status_code = 404
        fp = open("doc_root/notfound.html", "r")
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:notice] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))

    try:
        # read file and create response
        file_content = fp.read()
        fp.close()
        extn = file_name.split('.')[-1]

        response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
        response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
        response += "Date: " + get_current_GMTtime() + "\r\n"
        response += "Connection: close\r\n"
        response += "Set-Cookie: " + set_cookie(CLIENT_IP) + "\r\n"

        l_mod_time = get_last_modification_time(file_name)
        if l_mod_time:
            response += "Last-Modified: " + l_mod_time + "\r\n"
        response += "Content-Length: " + str(len(file_content)) + "\r\n" 
        response += "Content-Language: en-US\r\n"
        # if it is image file, then set 'Content-Type' header
        if is_image_file:
            if extn == 'jpg':
                extn = 'jpeg'
            response += "Content-Type: image/" + extn + "\r\n"
            response += "\r\n"
            response_encoded = response.encode() + file_content
        else:
            response += "Content-Type: text/html\r\n"
            response += "\r\n" + file_content
            response_encoded = response.encode()
        user_agent = "-"
        if "User-Agent" in request_headers:
            user_agent = request_headers["User-Agent"]
        logging.info(" {} \"GET {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, len(file_content), user_agent))
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))
    return response, response_encoded

def HEAD_method(CLIENT_IP, request_path, request_version, request_headers):

    global CONFIG, STATUS_CODE_MESSAGES
    # flag
    is_image_file = False
    doc_root_path = CONFIG['ROOT']['PATH']
    status_code = 200

    # get relative file path
    if request_path == "":
        return
    file_name = ""
    if request_path == '/':
        file_name = "/index.html"
    else:
        file_name = request_path
    file_name = doc_root_path + file_name
    # print(file_name)
    try:
        # check for file existence and access (here for GET, read access)
        if os.access(file_name, os.F_OK) and not os.access(file_name, os.R_OK):
            # create forbidden response
            status_code = 403
            entity = "<!DOCTYPE html><head><title>Access Error</title></head><body><h1>403 Forbidden</h1><p>Access denied</p></body></html>"
            response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
            response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
            response += "Date: " + get_current_GMTtime() + "\r\n"
            response += "Connection: close\r\n"
            response += "Content-Length: " + str(len(entity)) + "\r\n" 
            response += "Content-Language: en-US\r\n"
            response += "Content-Type: text/html\r\n"
            response += "\r\n"
            response_encoded = response.encode()
            user_agent = "-"
            if "User-Agent" in request_headers:
                user_agent = request_headers["User-Agent"]
            logging.info(" {} \"HEAD {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, len(entity), user_agent))
            return response, response_encoded

        # compare time in 'If-Modified-Since' header and file's modification time
        if os.access(file_name, os.F_OK) and "If-Modified-Since" in request_headers:
            t1 = time.strptime(get_last_modification_time(file_name).strip(), "%a, %d %b %Y %H:%M:%S GMT")
            t2 = time.strptime(request_headers["If-Modified-Since"].strip(), "%a, %d %b %Y %H:%M:%S GMT")
            if t1 < t2:
                status_code = 304
                response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
                response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
                response += "Date: " + get_current_GMTtime() + "\r\n"
                response += "Connection: close\r\n"
                response += "Set-Cookie: " + set_cookie(CLIENT_IP) + "\r\n"
                response += "\r\n"
                response_encoded = response.encode()

                # write into log file
                user_agent = "-"
                if "User-Agent" in request_headers:
                    user_agent = request_headers["User-Agent"]
                logging.info(" {} \"HEAD {} {}\" {} \"-\" \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, user_agent))
                return response, response_encoded
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))
    try:
        # if someone requests notfound.html files
        if request_path == "/notfound.html":
            raise FileNotFoundError
        # to handle image files
        if file_name.split('.')[-1] in ['jpg', 'jpeg', 'png', 'webp', 'gif']:
            fp = open(file_name, "rb")
            is_image_file = True
        else:
            fp = open(file_name, "r")
    except FileNotFoundError as err:
        status_code = 404
        fp = open("doc_root/notfound.html", "r")
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))

    try:
        # read file and create response
        file_content = fp.read()
        fp.close()
        extn = file_name.split('.')[-1]

        response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
        response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
        response += "Date: " + get_current_GMTtime() + "\r\n"
        response += "Connection: close\r\n"
        response += "Set-Cookie: " + set_cookie(CLIENT_IP) + "\r\n"

        l_mod_time = get_last_modification_time(file_name)
        if l_mod_time:
            response += "Last-Modified: " + l_mod_time + "\r\n"
        response += "Content-Length: " + str(len(file_content)) + "\r\n" 
        response += "Content-Language: en-US\r\n"

        # if it is image file, then set 'Content-Type' header
        if is_image_file:
            if extn == 'jpg':
                extn = 'jpeg'
            response += "Content-Type: image/" + extn + "\r\n"
            response += "\r\n"
            response_encoded = response.encode()
        else:
            response += "Content-Type: text/html\r\n"
            response += "\r\n"
            response_encoded = response.encode()

        user_agent = "-"
        if "User-Agent" in request_headers:
            user_agent = request_headers["User-Agent"]
        logging.info(" {} \"HEAD {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, len(file_content), user_agent))
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))
    return response, response_encoded

def POST_method(CLIENT_IP, request_path, request_version, request_headers, request_body):
    global CONFIG, STATUS_CODE_MESSAGES
    status_code = 200
    doc_root_path = CONFIG['ROOT']['PATH']
    file_name = "post_data.csv"
    fields_in_csv = ['client_ip', 'date_time', 'is_multipart(Y/N)', 'name', 'filename', 'value']

    if "Expect" in request_headers and request_headers["Expect"] == "100-continue":
        # if specified Content-Length supported by server, then 100, else 417
        if int(request_headers["Content-Length"]) <= int(CONFIG['CONTENT']['LENGTH']):
            status_code = 100
        else:
            status_code = 417
        
        response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
        response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
        response += "Date: " + get_current_GMTtime() + "\r\n"
        response += "Connection: close\r\n"
        response += "\r\n"

        response_encoded = response.encode()

        # if Expectation Failed, then write into log file as error
        if status_code == 417:
            process_id = str(os.getpid())
            thread_id = str(threading.current_thread().ident)
            logging.error("[core:notice] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, "Expectation Failed"))
        user_agent = "-"
        if "User-Agent" in request_headers:
            user_agent = request_headers["User-Agent"]
        logging.info(" {} \"POST {} {}\" {} \"-\" \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, user_agent))
        return response, response_encoded

    try:
        if os.access(file_name, os.F_OK):
            fp = open(file_name, "a")
            csv_writer_obj = csv.writer(fp)
        else:
            fp = open(file_name, "w")
            csv_writer_obj = csv.writer(fp)
            csv_writer_obj.writerow(fields_in_csv)
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))

    try:
        if "Content-Type" in request_headers:
            if "application/x-www-form-urlencoded" in request_headers["Content-Type"]:
                req_entity = request_body[0]
                all_data = list()
                pairs = req_entity.split('&')
                date_time = get_current_GMTtime()

                # save records
                for each in pairs:
                    key, value = each.split('=', 1)
                    # resolve percent encoding in value
                    value = decode_percent_encoded_data(value)
                    all_data = [CLIENT_IP, date_time, 'N', key, None, value]
                    csv_writer_obj.writerow(all_data)
                fp.close()

                status_code = 200
                entity = "<!DOCTYPE html><head><title>Success</title></head><body><h1>200 OK</h1><p>Server recorded your data.</p></body></html>"
                response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
                response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
                response += "Date: " + date_time + "\r\n"
                response += "Connection: close\r\n"
                response += "Content-Length: " + str(len(entity)) + "\r\n" 
                response += "Content-Language: en-US\r\n"
                response += "Content-Type: text/html\r\n"
                response += "\r\n" + entity 
                response_encoded = response.encode()
                user_agent = "-"
                if "User-Agent" in request_headers:
                    user_agent = request_headers["User-Agent"]
                logging.info(" {} \"POST {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, len(entity), user_agent))
                return response, response_encoded


            elif "multipart/form-data" in request_headers["Content-Type"]:
                # get segregated data from request body
                seg_data = parse_multipart_data(request_headers, request_body)
                # print(seg_data)
                all_data = list()
                date_time = get_current_GMTtime()
                post_file_path = ""
                all_file_locations = []

                for each_data in seg_data:                    
                        
                    if each_data[1] != None:
                        status_code = 201
                        post_file_path = doc_root_path + request_path
                        if not os.access(post_file_path, os.F_OK):
                            os.mkdir(post_file_path)

                        post_fp = open(post_file_path + '/' + each_data[1], "w")
                        post_fp.write(each_data[2])
                        post_fp.close()

                        # to handle 'Location' header
                        all_file_locations.append("http://localhost:" + CONFIG['DEFAULT_VALUES']['PORT'] + request_path + '/' + each_data[1])
                    
                    all_data = [CLIENT_IP, date_time, 'Y', each_data[0], each_data[1], each_data[2]]
                    csv_writer_obj.writerow(all_data)
                fp.close()

                if status_code == 200:
                    entity = "<!DOCTYPE html><head><title>Success</title></head><body><h1>200 OK</h1><p>Server recorded your data.</p></body></html>"
                elif status_code == 201:
                    entity = "<!DOCTYPE html><head><title>Success</title></head><body><h1>201 Created</h1><p>File Created.</p></body></html>"
                
                response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
                response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
                response += "Date: " + date_time + "\r\n"
                response += "Connection: close\r\n"
                response += "Content-Length: " + str(len(entity)) + "\r\n" 
                response += "Content-Language: en-US\r\n"
                response += "Content-Type: text/html\r\n"
                # if 201 Created, then specify 'Location' header
                if status_code == 201:
                    response += "Location: " + '; '.join(all_file_locations) + "\r\n"
                response += "\r\n" + entity 
                response_encoded = response.encode()
                user_agent = "-"
                if "User-Agent" in request_headers:
                    user_agent = request_headers["User-Agent"]
                logging.info(" {} \"POST {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, len(entity), user_agent))
                return response, response_encoded
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))


def DELETE_method(CLIENT_IP, request_path, request_version, request_headers):
    global CONFIG, STATUS_CODE_MESSAGES
    doc_root_path = CONFIG['ROOT']['PATH']
    status_code = None
    entity = ""
    try:
        if request_path == "":
            return
        file_name = ""
        if request_path == '/':
            file_name = "/index.html"
        else:
            file_name = request_path
        file_name = doc_root_path + file_name

        # check existance of file
        if os.access(file_name, os.F_OK):
            # deny access as per this condition
            if file_name == "doc_root/index.html" or file_name == "doc_root/notfound.html" or not os.access(file_name, os.W_OK):
                status_code = 403
                entity = "<!DOCTYPE html><head><title>Access Error</title></head><body><h1>403 Forbidden</h1><p>Access denied</p></body></html>"
            else:
                os.remove(file_name)
                status_code = 200
                entity = "<!DOCTYPE html><head><title>Success</title></head><body><h1>Resource deleted</h1></body></html>"
        else:
            # if file is not found
            status_code = 404
            fp = open("doc_root/notfound.html", "r")
            entity = fp.read()
            fp.close()

        response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
        response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
        response += "Date: " + get_current_GMTtime() + "\r\n"
        response += "Connection: close\r\n"
        response += "Content-Length: " + str(len(entity)) + "\r\n" 
        response += "Content-Language: en-US\r\n"
        response += "Content-Type: text/html\r\n"
        response += "\r\n" + entity
        response_encoded = response.encode()
        user_agent = "-"
        if "User-Agent" in request_headers:
            user_agent = request_headers["User-Agent"]
        logging.info(" {} \"DELETE {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_path, request_version, status_code, len(entity), user_agent))
        return response, response_encoded

    except Exception as err:
        # writing error into log file
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))

# function to handle thread for each new connection
def new_client(client_sock, client_address):
    global STATUS_CODE_MESSAGES, CONFIG
    CLIENT_IP = client_address[0]

    try:
        status_code = None
        # recieve request from connected client
        request = client_sock.recv(2048).decode()

        # get request method, HTTP version, requested path
        request_line_headers = request.split('\r\n\r\n')[0]
        request_body = request.split('\r\n\r\n')[1:]
        request_line = request_line_headers.split('\r\n')[0].split()
    
        request_method = request_line[0]
        request_path = request_line[1]
        request_version = request_line[2]

        request_headers = dict()
        
        # get all headers in request
        for each in request_line_headers.split('\r\n')[1:]:
            key, value = each.split(':', 1)
            request_headers[key.strip()] = value.strip()
    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))

    try:
        # if version not satisfied
        if request_version != "HTTP/1.1":
            status_code = 505
            entity = "<!DOCTYPE html><head><title>Version Error</title></head><body><h1>505 HTTP Version Not Supported</h1><p>Supported version : HTTP/1.1</p></body></html>"
            response = "HTTP/1.1" + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
            response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
            response += "Date: " + get_current_GMTtime() + "\r\n"
            response += "Connection: close\r\n"
            response += "Content-Length: " + str(len(entity)) + "\r\n" 
            response += "Content-Language: en-US\r\n" 
            response += "\r\n" + entity
            response_encoded = response.encode()

            # send response to the client
            client_sock.sendall(response_encoded)
            # close the connection
            client_sock.close()
            user_agent = "-"

            # write data into log file
            if "User-Agent" in request_headers:
                user_agent = request_headers["User-Agent"]
            process_id = str(os.getpid())
            thread_id = str(threading.current_thread().ident)
            logging.error("[core:notice] [pid {}:tid {}] [client {}] HTTP Version not supported".format(process_id, thread_id, CLIENT_IP))
            logging.info(" {} \"{} {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_method, request_path, "HTTP/1.1", status_code, len(entity), user_agent))
            return
        
        # if URI in requested path is not supported by server
        if len(request_path)-1 > int(CONFIG['URI']['LENGTH']):
            status_code = 414
            entity = "<!DOCTYPE html><head><title>URI Error</title></head><body><h1>414 Request-URI Too Long</h1><p>Supported URI length : 120</p></body></html>"
            response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
            response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
            response += "Date: " + get_current_GMTtime() + "\r\n"
            response += "Connection: close\r\n"
            response += "Content-Length: " + str(len(entity)) + "\r\n"
            response += "\r\n" + entity
            response_encoded = response.encode()
            # send response to the client
            client_sock.sendall(response_encoded)
            # close the connection
            client_sock.close()
            user_agent = "-"

            # write data into log file
            if "User-Agent" in request_headers:
                user_agent = request_headers["User-Agent"]
            process_id = str(os.getpid())
            thread_id = str(threading.current_thread().ident)
            logging.error("[core:notice] [pid {}:tid {}] [client {}] Request-URI Too Long".format(process_id, thread_id, CLIENT_IP))
            logging.info(" {} \"{} {} {}\" {} {} \"-\" \"{}\"".format(CLIENT_IP, request_method, request_path, request_version, status_code, len(entity), user_agent))
            return

        # check method in request and call appropriate function
        if request_method == "GET":
            response, response_encoded = GET_method(CLIENT_IP, request_path, request_version, request_headers)
        elif request_method == "HEAD":
            response, response_encoded = HEAD_method(CLIENT_IP, request_path, request_version, request_headers)
        elif request_method == "POST":
            response, response_encoded = POST_method(CLIENT_IP, request_path, request_version, request_headers, request_body)
        # elif request_method == "PUT":
        #     pass
        elif request_method == "DELETE":
            response, response_encoded = DELETE_method(CLIENT_IP, request_path, request_version, request_headers)

        elif request_method not in ['GET', 'HEAD', 'POST', 'DELETE']:
            # method not allowed
            status_code = 405
            response = request_version + " " + str(status_code) + " " + STATUS_CODE_MESSAGES[str(status_code)] + "\r\n"
            response += "Server: myServer/0.0.1 (Ubuntu)\r\n"
            response += "Date: " + get_current_GMTtime() + "\r\n"
            response += "Connection: close\r\n"
            response += "Content-Length: 0\r\n" 
            response += "Content-Language: en-US\r\n" 
            response += "Allow: GET, HEAD, POST, DELETE\r\n"
            response += "\r\n"
            response_encoded = response.encode()

            # write as error into log file
            process_id = str(os.getpid())
            thread_id = str(threading.current_thread().ident)
            logging.error("[core:notice] [pid {}:tid {}] [client {}] Method Not Allowed".format(process_id, thread_id, CLIENT_IP))
            
        # send response to the client
        client_sock.sendall(response_encoded)

        # close the connection
        client_sock.close()

    except Exception as err:
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, CLIENT_IP, err))

if __name__ == "__main__":
    
    # read config file
    CONFIG = configparser.ConfigParser()
    CONFIG.read("config.ini")

    # extract servername and port number 
    SERVER_NAME = CONFIG['DEFAULT_VALUES']['NAME']
    SERVER_PORT = int(CONFIG['DEFAULT_VALUES']['PORT'])

    # create server socket (TCP socket)
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # to reuse the same address for socket
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # bind the socket to port number
    server_socket.bind((SERVER_NAME, SERVER_PORT))
    # to listen to connections
    server_socket.listen(1)


    # start one thread which is required for handling cookies
    # th = threading.Thread(target=handle_cookies, args=())
    # th.start()

    # create log directory if not exist
    if not os.access(CONFIG['LOG']['DIR_PATH'], os.F_OK):
        os.mkdir(CONFIG['LOG']['DIR_PATH'])
    # basic configuration for log file
    logging.basicConfig(filename=CONFIG['LOG']['FILE_PATH'], format='[%(asctime)s]: %(message)s', level=logging.DEBUG)

    is_printed = False 
    try:
        while True:
            # go on accepting connections from clients
            client_sock, client_address = server_socket.accept()
            # print(client_address, "connected...")
            
            # if max-allowed-connections exceeded limit, then send Service Unavailable
            if threading.active_count() > int(CONFIG['MAX']['CONNECTIONS']):
                if not is_printed:
                    # print("Connections exceeded the maximum limit")
                    is_printed = True
                st_code = 503
                response = "HTTP/1.1 " + str(st_code) + " " + STATUS_CODE_MESSAGES[str(st_code)] + "\r\n"
                response += "Retry After: " + str(random.randint(100, 200)) + "\r\n"
                response += "Date: " + get_current_GMTtime() + "\r\n"
                response += "Connection: close\r\n\r\n"
                client_sock.sendall(response.encode())
                client_sock.close()
            else:
                # make new thread for new client
                thread = threading.Thread(target=new_client, args=(client_sock, client_address,))
                # start new thread
                thread.start()
    except KeyboardInterrupt as err:
        # print("\nForced exit...")
        process_id = str(os.getpid())
        thread_id = str(threading.current_thread().ident)
        logging.error("[core:error] [pid {}:tid {}] [client {}] {}".format(process_id, thread_id, "-", err))
        sys.exit(1)

