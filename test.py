from socket import *
import threading
import time
from colorama import Fore, Style

# Testing file

# get current GMT time (in HTTP format - rfc1123)
def get_current_GMTtime():
    return time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime())

# method not allowed
def send_unknown_method():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "FIND / HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# HTTP version not supported
def send_unknown_version():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET / HTTP/0.8\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# long uri 414
def send_long_uri():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    uri = 'a'*50 + 'b'*50 + 'c'*50 + '.html'
    req = "GET " + uri + " HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# index.html
def send_get1():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET / HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# aboutus.html
def send_get2():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET /aboutus.html HTTP/1.1\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# image.jpg
def send_get3():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET /image.jpg HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048)
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    # print(msg)
    print("Could not print as response body is too large!!")
    print("\n" + '_'*100 + "\n")
    csock.close()

# image3.gif
def send_get4():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET /image3.gif HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048)
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    # print(msg)
    print("Could not print as response body is too large!!")
    print("\n" + '_'*100 + "\n")
    csock.close()

# notfound.html
def send_get5():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET /xyz.html HTTP/1.1\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# forbidden for file1.txt
def send_get6():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET /file1.txt HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# if-modified-since for file1.txt
def send_get7():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "GET / HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "If-Modified-Since: " + get_current_GMTtime() + "\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()


# index file
def send_head1():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "HEAD / HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# notfound.html
def send_head2():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))
    req = "HEAD /abcd.pdf HTTP/1.1\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Connection: close\r\n"
    req += "\r\n"
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# post application/x-www-form-urlencoded
def send_post1():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "POST /test HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Content-Type: application/x-www-form-urlencoded\r\n"
    req += "Content-Length: 27\r\n"
    req += "\r\n"
    req += "Name=Gareth+Wylie&Age=24&Formula=a%2Bb+%3D%3D+21"

    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# post application/x-www-form-urlencoded
def send_post2():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "POST /test HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += 'Content-Type: multipart/form-data;boundary="boundary"\r\n\r\n'
    req += '--boundary\r\nContent-Disposition: form-data; name="field1"\r\n\r\n'
    req += 'value1\r\n'
    req += '--boundary\r\nContent-Disposition: form-data; name="field2"; filename="hello.txt"\r\n\r\n'
    req += 'This is hello.txt\r\n'
    req += '--boundary--'

    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# post application/x-www-form-urlencoded
def send_post3():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "POST /test HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += 'Content-Type: multipart/form-data;boundary="###"\r\n\r\n'
    req += '--###\r\nContent-Disposition: form-data; name="field1"\r\n\r\n'
    req += 'value1\r\n'
    req += '--###\r\nContent-Disposition: form-data; name="field2"; filename="example.txt"\r\n\r\n'
    req += 'This is example.txt\r\n'
    req += '--###\r\nContent-Disposition: form-data; name="description"; filename="new_example.txt"\r\n\r\n'
    req += 'Hello, this is new_example.txt file.\r\n'
    req += '--###--'

    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# expect 100 continue
def send_post4():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "POST /test HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Expect: 100-continue\r\n"
    req += "Content-Type: application/x-www-form-urlencoded\r\n"
    req += "Content-Length: 2778\r\n"
    req += "\r\n"
    
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()


# expect 417 expectation failed
def send_post5():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "POST /test HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "Expect: 100-continue\r\n"
    req += "Content-Type: application/x-www-form-urlencoded\r\n"
    req += "Content-Length: 3412784589\r\n"
    req += "\r\n"
    
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# delete 200
def send_delete1():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "DELETE /del1.txt HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "\r\n"
    
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# delete 403
def send_delete2():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "DELETE /del2.txt HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "\r\n"
    
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()

# delete 404
def send_delete3():
    csock = socket(AF_INET, SOCK_STREAM)
    csock.connect(('', 12500))

    req = "DELETE /del10.txt HTTP/1.1\r\n"
    req += "Host: foo.example\r\n"
    req += "User-Agent: autotesting.py\r\n"
    req += "\r\n"
    
    csock.send(req.encode())
    msg = csock.recv(2048).decode()
    print(Fore.GREEN + req)
    print(Style.RESET_ALL)
    print('*'*50 + "\n")
    print(msg)
    print("\n" + '_'*100 + "\n")
    csock.close()


if __name__=="__main__":
    print("Printing Requests in "+ Fore.GREEN + "GREEN", end="")
    print(Style.RESET_ALL)
    print("and")
    print("Printing Responses in NORMAL font-color")
    print('*'*50 + "\n")

    # calling proper functions for testing and printing request and response recieved
    send_unknown_method()
    send_unknown_version()
    send_long_uri()

    send_get1()
    send_get2()
    send_get3()
    send_get4()
    send_get5()
    send_get6()
    send_get7()

    send_head1()
    send_head2()

    send_post1()
    send_post2()
    send_post3()
    send_post4()
    send_post5()

    send_delete1()
    send_delete2()
    send_delete3()
