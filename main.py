import sys
import os
import signal

def main():
    try:
        # if command line args not satisfied, then raise Exception to display instructions
        if len(sys.argv) != 2:
            raise Exception

        # invoking os.system() to run server in background
        if sys.argv[1] == "start" or sys.argv[1] == "START":
            os.system('sudo python3 http_server.py &')
            print("Server started")

        # killing all current running instances to stop server
        elif sys.argv[1] == "stop" or sys.argv[1] == "STOP":
            all_processes = os.popen(f"ps ax | grep http_server.py | grep -v grep")
            for each in all_processes:
                process_id = int(each.split()[0])
                os.kill(process_id, signal.SIGKILL)
            print("Server stopped")

        # restart is stopping and starting server again
        elif sys.argv[1] == "restart" or sys.argv[1] == "RESTART":
            all_processes = os.popen(f"ps ax | grep http_server.py | grep -v grep")
            for each in all_processes:
                process_id = int(each.split()[0])
                os.kill(process_id, signal.SIGKILL)
            os.system('sudo python3 http_server.py &')
            print("Server restarted")

        else:
            raise Exception
    except Exception:
        # print instructions
        print("\nRefer following commands:")
        print("\tTo start server : sudo python3 main.py (start | START)")
        print("\tTo stop server : sudo python3 main.py (stop | STOP)")
        print("\tTo restart server : sudo python3 main.py (restart | RESTART)\n")
        sys.exit(1)

if __name__ == "__main__":
    main()